﻿Start();


void Start()
{

    Console.WriteLine("Press the space bar to start file mixing every 10 seconds.");
    string filename = "D:\\Labs\\Системне та мережеве\\Lab1\\data.txt";
    bool spaceBarPressed = false;
    while (true)
    {
        if (Console.KeyAvailable)
        {
            var key = Console.ReadKey(intercept: true).Key;
            if (key == ConsoleKey.Spacebar)
            {
                spaceBarPressed = true;
                break;
            }
        }
    }

    if (spaceBarPressed)
    {
        MixDataInFile(filename);

    }

    Console.ReadLine();

}

void MixDataInFile(string filename)
{
    int[] data = ReadDataFromFile(filename);
    data = MixData(data);
    SaveDataToFile(filename, data);
}

int[] ReadDataFromFile(string filename)
{


    int[] data;
    if (!File.Exists(filename))
    {
        Console.WriteLine("Data file does not exist. ");
        return new int[0];
    }

    string[] lines = File.ReadAllLines(filename);
    data = new int[lines.Length];

    for (int i = 0; i < lines.Length; i++)
    {
        if (!int.TryParse(lines[i], out int value))
        {
            Console.WriteLine($"Error: Invalid data at line {i + 1}");
            return new int[0];
        }

        data[i] = value;
    }

    return data;
}

void SaveDataToFile(string filename, int[] data)
{
    using (StreamWriter writer = new StreamWriter(filename))
    {
        foreach (int value in data)
        {
            writer.WriteLine(value);
        }
    }
}

int[] MixData(int[] data)
{
    Random random = new Random();
    data = data.OrderBy(x => random.Next()).ToArray();
    return data;
}


