﻿
Start();


void Start()
{

    Console.WriteLine("Press the space bar to start sorting the data.");
    string fileName = "D:\\Labs\\Системне та мережеве\\Lab1\\data.txt";
    bool spaceBarPressed = false;
    while (true)
    {
        if (Console.KeyAvailable)
        {
            var key = Console.ReadKey(intercept: true).Key;
            if (key == ConsoleKey.Spacebar)
            {
                spaceBarPressed = true;
                break;
            }
        }
    }

    if (spaceBarPressed)
    {
        int[] data = ReadDataFromFile(fileName);
        Console.WriteLine("Sorting the data...");
        SortData(fileName,data);
        Console.WriteLine("Work completed");
    }

    Console.ReadLine();

}


int[] ReadDataFromFile(string filename)
{


    int[] data;
    if (!File.Exists(filename))
    {
        Console.WriteLine("Data file does not exist. ");
        return new int[0];
    }

    string[] lines = File.ReadAllLines(filename);
    data = new int[lines.Length];

    for (int i = 0; i < lines.Length; i++)
    {
        if (!int.TryParse(lines[i], out int value))
        {
            Console.WriteLine($"Error: Invalid data at line {i + 1}");
            return new int[0];
        }

        data[i] = value;
    }

    return data;
}

void SaveDataToFile(string filename, int[] data)
{
    using (StreamWriter writer = new StreamWriter(filename))
    {
        foreach (int value in data)
        {
            writer.WriteLine(value);
        }
    }
}

void SortData(string filename,int[] data)
{

    bool swapped;
    do
    {
        swapped = false;
        for (int i = 0; i < data.Length - 1; i++)
        {
            if (data[i] > data[i + 1])
            {
                int temp = data[i];
                data[i] = data[i + 1];
                data[i + 1] = temp;
                swapped = true;
            }

            
        }
        Thread.Sleep(500);
        OutputArray(data);
        SaveDataToFile(filename, data);
    } while (swapped);
}

void OutputArray(int[] data)
{
    for (int i = 0; i < data.Length - 1; i++)
    {
        Console.Write(data[i]+", ");
    }
    Console.WriteLine();
}
