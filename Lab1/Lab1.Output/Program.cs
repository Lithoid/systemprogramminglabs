﻿// See https://aka.ms/new-console-template for more information
Start();
void Start()
{


    while (true)
    {
        Console.Clear();
        int[] data = ReadDataFromFile("D:\\Labs\\Системне та мережеве\\Lab1\\data.txt");
        OutputArray(data);

        Thread.Sleep(300);
    }
}
int[] ReadDataFromFile(string filename)
{


    int[] data;
    if (!File.Exists(filename))
    {
        Console.WriteLine("Data file does not exist. ");
        return new int[0];
    }

    string[] lines = File.ReadAllLines(filename);
    data = new int[lines.Length];

    for (int i = 0; i < lines.Length; i++)
    {
        if (!int.TryParse(lines[i], out int value))
        {
            Console.WriteLine($"Error: Invalid data at line {i + 1}");
            return new int[0];
        }

        data[i] = value;
    }

    return data;
}


void OutputArray(int[] data)
{
    for (int i = 0; i < data.Length - 1; i++)
    {
        for (int j = 0; j < data[i]; j++)
        {
            Console.Write("*");
        }
        Console.WriteLine();
    }
    

}
