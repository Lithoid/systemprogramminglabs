﻿
using System.Management;
// 1. List of all logical disks
var logicalDisks = DriveInfo.GetDrives();
Console.WriteLine("Logical Disks:");
foreach (var disk in logicalDisks)
{
    Console.WriteLine($"Drive: {disk.Name}");
}

Console.WriteLine();

// 2. Get the type and explanation of each disk
Console.WriteLine("Disk Types:");
foreach (var disk in logicalDisks)
{
    Console.WriteLine($"Drive: {disk.Name}");
    Console.WriteLine($"Type: {disk.DriveType}");

    switch (disk.DriveType)
    {
        case DriveType.Fixed:
            Console.WriteLine("Explanation: The disk is a fixed (hard) disk.");
            break;
        case DriveType.CDRom:
            Console.WriteLine("Explanation: The disk is an optical disc (CD/DVD).");
            break;
        case DriveType.Removable:
            Console.WriteLine("Explanation: The disk is a removable storage device (USB drive, flash drive, etc.).");
            break;
        default:
            Console.WriteLine("Explanation: The disk type is unknown.");
            break;
    }

    Console.WriteLine();
}

// 3. Get information about disks and file systems
Console.WriteLine("Disk Information:");
foreach (var disk in logicalDisks)
{

    try
    {
        Console.WriteLine($"Drive: {disk.Name}");
        Console.WriteLine($"File System: {disk.DriveFormat}");
        Console.WriteLine();
    }
    catch (Exception e)
    {

        Console.WriteLine(e.InnerException);
    }

}

// 4. Get information about occupancy and free space on each disk
Console.WriteLine("Disk Space Information:");
foreach (var disk in logicalDisks)
{
   
    try
    {
        Console.WriteLine($"Drive: {disk.Name}");
        Console.WriteLine($"Total Size: {disk.TotalSize} bytes");
        Console.WriteLine($"Free Space: {disk.TotalFreeSpace} bytes");
        Console.WriteLine();

    }
    catch (Exception e)
    {

        Console.WriteLine(e.InnerException);
    }
}

// 5. Get information about system memory
var memory = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem").Get().Cast<ManagementObject>().First();
var totalMemory = Convert.ToInt64(memory["TotalVisibleMemorySize"]);
var freeMemory = Convert.ToInt64(memory["FreePhysicalMemory"]);
Console.WriteLine("System Memory Information:");
Console.WriteLine($"Total Memory: {totalMemory} KB");
Console.WriteLine($"Free Memory: {freeMemory} KB");
Console.WriteLine();

// 6. Get computer name
Console.WriteLine("Computer Name:");
Console.WriteLine(Environment.MachineName);
Console.WriteLine();

// 7. Get current user name
Console.WriteLine("Current User:");
Console.WriteLine(Environment.UserName);
Console.WriteLine();

// 8. Get information about system directory, temporary directory, and current working directory
Console.WriteLine("System Directories:");
Console.WriteLine($"System Directory: {Environment.SystemDirectory}");
Console.WriteLine($"Temporary Directory: {Path.GetTempPath()}");
Console.WriteLine($"Current Working Directory: {Environment.CurrentDirectory}");

Console.ReadLine();