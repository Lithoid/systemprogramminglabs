﻿
using System.Collections;

Start();


void Start()
{

    Console.WriteLine("Press the space bar to start file creation.");
    string filename = "D:\\Labs\\Системне та мережеве\\Lab1\\data.txt";
    bool spaceBarPressed = false;
    while (true)
    {
        if (Console.KeyAvailable)
        {
            var key = Console.ReadKey(intercept: true).Key;
            if (key == ConsoleKey.Spacebar)
            {
                spaceBarPressed = true;
                break;
            }
        }
    }

    if (spaceBarPressed)
    {
        CreateFile(filename);

        Console.WriteLine("Work completed");
    }

    Console.ReadLine();

}

void CreateFile(string filename)
{

    Console.WriteLine("Data file does not exist. Creating a new file with random data.");

    //int[] data = GenerateRandomData(10);
    int[] data =  GenerateDistinctRandomData(30);
    SaveDataToFile(filename, data);

}




void SaveDataToFile(string filename, int[] data)
{
    using (StreamWriter writer = new StreamWriter(filename))
    {
        foreach (int value in data)
        {
            writer.WriteLine(value);
        }
    }
}

int[] GenerateRandomData(int count)
{
    Random random = new Random();
    int[] data = new int[count];

    for (int i = 0; i < count; i++)
    {
        data[i] = random.Next(1, 10);
    }

    return data;
}

int[] GenerateDistinctRandomData(int maxValue)
{
    Random random = new Random();
    int[] data =  Enumerable.Range(1, maxValue).OrderBy(x => random.Next()).ToArray();
    return data;
}



